const mysql = require('mysql2')

const openConnection = () =>{
   const connection = mysql.createConnection({

        host: 'moviecontainer',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'movies'
    })
    
    connection.connect();
    return connection
}

module.exports = {
    openConnection
}