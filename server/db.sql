create table movie (
  movie_id integer primary key auto_increment,
  movie_title VARCHAR(200),
  movie_release_date VARCHAR(200),
  movie_time VARCHAR(200),
  director_name VARCHAR(200)
);

INSERT INTO movie VALUES(1, "Pushpa", "20 Dec 2021", "180 min", "Karan Johar");
INSERT INTO movie VALUES(2, "Interstellar", "20 Jan 2014", "150 min", "Chris Nolan");