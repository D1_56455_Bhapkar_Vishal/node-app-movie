const express = require('express')
const db = require('./db')


const app = express();
app.use(express.json())


//get movie details
app.get("/", (request, response)=>{
    const connection = db.openConnection();
    const sql = `SELECT * FROM movie`
    connection.query(sql, (error, data)=>{
        connection.end()
        response.send(data);
    })
})


//post new movie details
app.post("/", (request, response)=>{
    const connection = db.openConnection();
    const {movie_id, movie_title, movie_release_date, movie_time, director_name } = request.body
    const sql = `INSERT INTO movie VALUES(${movie_id}, '${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}')`
    connection.query(sql, (error, data)=>{
        connection.end()
        response.send(data)
    })
})

//update movie details
app.put("/:movie_id", (request, response)=>{
    const connection = db.openConnection();
    const { movie_id } = request.params
    const {movie_title, director_name} = request.body
    const sql = `UPDATE movie SET movie_title = '${movie_title}', director_name = '${director_name}' WHERE movie_id = ${movie_id}`
    connection.query(sql, (error, data)=>{
        connection.end()
        response.send(data)
    })
})

//Delete Movie
app.delete("/:movie_id", (request, response)=>{
    const connection = db.openConnection();
    const { movie_id } = request.params
   
    const sql = `DELETE FROM movie WHERE movie_id = ${movie_id}`

    connection.query(sql, (error, data)=>{
        connection.end()
        response.send(data)
    })
})


app.listen(4000, ()=>{

    console.log("port started at 4000!!!")
})

